package com.company;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Eugen on 4/5/2017.
 */

public class HumanUtil {
    public enum Age {OLD, YOUNG};

    public static Human searchByStreetName(List<Human> list, Address address) {
        for(Human h : list) {
            if((h.getAddress().getStreet()).equals(address.getStreet())) {
                return h;
            }
        }
        return new Human();
    }


    public static Human searchByName(List<Human> list, String name) {
        for(Human h : list) {
            if(h.getName().equals(name)) {
                return h;
            }
        }
        return new Human();
    }

    public static Human searchByAge(List<Human> list, Age age) {
        if(age.equals(Age.OLD)) {
            return list.get(0);
        }
        if(age.equals(Age.YOUNG)) {
            return list.get(list.size() - 1);
        }
        return new Human();
    }


    public static List searchPeopleOnTheSameStreet(List<Human> list, Address address) {
        List<Human> returnList = new ArrayList<>();
        for(Human h : list) {
            if(h.getAddress().getStreet().equals(address.getStreet())) {
                returnList.add(h);
            }
        }
        return returnList;
    }

    public static Human searchByAddressAttrib(List<Human> list, Address address) {
        if(address.getStreet() != null) {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getStreet())) {
                    return h;
                }
            }
        }
        else {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getHouseId())) {
                    System.out.println(h.toString());
                    return h;
                }
            }
        }
        return new Human();
    }

    public static List searchInDateRange(List<Human> list, Calendar leftRange, Calendar rightRange) {
        List<Human> returnList = new ArrayList<>();
        for(Human h : list) {
            double milTime = h.getBirthDate().getTimeInMillis();
            if(milTime > leftRange.getTimeInMillis() &&
                    milTime < rightRange.getTimeInMillis()) {
                returnList.add(h);
            }
        }
        return returnList;
    }
}
