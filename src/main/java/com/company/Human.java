package com.company;


import sun.util.resources.cldr.aa.CalendarData_aa_DJ;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


public class Human implements Comparable<Human>{
    private String name;
    private String surname;
    private Calendar birthDate;
    private Address address;

    public Human() {}

    public Human(Address address) {
        this.address = address;
    }

    public Human(String name, String surname, Calendar birthDate, Address address) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public Calendar getBirthDate() {
        return this.birthDate;
    }

    public Address getAddress() {
        return this.address;
    }

    @Override
    public int compareTo(Human obj) {
        //return this.getBirthDate().compareTo(obj.getBirthDate());
        return this.birthDate.compareTo(obj.birthDate);
    }

    @Override
    public String toString() {
      /*  return "Name: " + getName() + " Surname: " + getSurname() +
                " BirthDate: " + getBirthDate().getTime() + " Address:Street: " + getAddress().getStreet() +
                " Address:HouseId: " + getAddress().getHouseId();*/
      return "Name: " + name + " Surname: " + surname + " BirthDate: " + birthDate.getTime() +
              " Street: " + address.getStreet() + " HouseId: " + address.getHouseId();
    }

    @Override
    public boolean equals(Object obj) {
        if(this.name == ((Human)obj).name) {
            return true;
        }
        return false;
    }


    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
