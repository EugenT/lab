package com.company;


public class Address {
    private String street;
    private int houseId;

    public Address(String street, int houseId) {
        this.street = street;
        this.houseId = houseId;
    }

    public Address(String street) {
        this.street = street;
    }

    public Address(int houseId) {
        this.houseId = houseId;
    }

    public void setStreet(String name) {
        this.street = name;
    }

    public void setHouseId(int id) {
        this.houseId = id;
    }

    public String getStreet() {
        return this.street;
    }

    public int getHouseId() {
        return this.houseId;
    }
}
