import com.company.Address;
import com.company.FibonacciGenerator;
import com.company.Human;
import com.company.HumanUtil;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Eugen on 4/5/2017.
 */
public class HumanUtilTest {
    List<Human> humanList = new ArrayList<>();
    @Before
    public void getToTestArray() {
        humanList.add(new Human("Stive","Thyganok",
                new GregorianCalendar(1998, 5, 19),
                new Address("street", 12)));
        humanList.add(new Human("Nancy","Somesurname",
                new GregorianCalendar(1988, 7, 11),
                new Address("GrowStreet", 12)));

        humanList.add(new Human("Jack","Jack",
                new GregorianCalendar(1977, 1, 4),
                new Address("street", 12)));

        humanList.add( new Human("Name1","Surname",
                new GregorianCalendar(2007, 1, 4),
                new Address("street", 12)));

        humanList.add(new Human("Name2","Surname2",
                new GregorianCalendar(1966, 1, 4),
                new Address("street", 12)));

        humanList.add(new Human("Name3","Surname",
                new GregorianCalendar(2003, 1, 4),
                new Address("street", 12)));

        humanList.add(new Human("Name4","Surname",
                new GregorianCalendar(2008, 1, 4),
                new Address("GrowStreet", 12)));

        Collections.sort(humanList);

    }

    //Test HumanUtil class methods
    @Test
    public void searchByNameTest() throws Exception {
        assertEquals("Stive", HumanUtil.searchByName(humanList, "Stive").getName());
    }

    @Test
    public void searchByStreetNameTest() throws Exception {
        assertEquals("GrowStreet", HumanUtil.searchByStreetName(humanList,
                new Address("GrowStreet")).getAddress().getStreet());
    }

    @Test
    public void searchByAgeTest() throws Exception {
        assertEquals(new GregorianCalendar(1966, 1, 4).getTime(),
                HumanUtil.searchByAge(humanList, HumanUtil.Age.OLD).getBirthDate().getTime());
    }

    @Test
    public void searchByAddressAtribTest() throws Exception {
        assertEquals("GrowStreet",
                HumanUtil.searchByAddressAttrib(humanList, new Address("GrowStreet")).getAddress().getStreet());
    }

    //Test Fibonacci class
    @Test
    public void fibonacciTest() throws Exception {
        int[] arr = FibonacciGenerator.getFibonacciSequance(5);
        assertEquals(arr[4], 3);
    }

    @Test
    public void searchInDateRangeTest() throws Exception {

    }

    @Test
    public void searchPeopleOnTheSameStreetTest() throws Exception {

    }
}
